include_guard ()

#[=======================================================================[.rst:
..command:: init_project

	Sets common settings for a project. These include:

	* all debug builds will have the postfix `-d`
	* the default install directory will be `${CMAKE_CURRENT_BINARY_DIR}/dist`
	* all output directories will include the current configuration
	* IDE folders will be turned on for visual studio

	::

		init_project()

#]=======================================================================]
macro (init_project)
	set (CMAKE_DEBUG_POSTFIX "-d")
	set (CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

	# Set the install root folder if not specified by the user
	if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
		set (
			CMAKE_INSTALL_PREFIX
			${CMAKE_CURRENT_BINARY_DIR}/dist
			CACHE PATH "Install root directory" FORCE
		)
		set (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT OFF)
	endif (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)

	if (NOT DEFINED CMAKE_RUNTIME_OUTPUT_DIRECTORY AND WIN32)
		set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/bin)
		set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/lib)
		set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/lib)
	endif ()

	if (CMAKE_GENERATOR MATCHES "Visual Studio")
		set_property (GLOBAL PROPERTY USE_FOLDERS ON)
	endif ()

	enable_testing ()
endmacro ()
