cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

include_guard ()

include (${CMAKE_CURRENT_LIST_DIR}/InitProject.cmake)

# Check for CPM
# set (CPM_DOWNLOAD_VERSION 0.40.5)
set(CPM_DOWNLOAD_VERSION 0.40.5)
file (GLOB CPM_MODULE_LOCATIONS ${CMAKE_BINARY_DIR}/cmake/CPM*.cmake)

set(CPM_NEEDS_DOWNLOAD FALSE)

if (NOT CPM_MODULE_LOCATIONS)
	set (CPM_NEEDS_DOWNLOAD TRUE)
else ()
	list (GET CPM_MODULE_LOCATIONS -1 CPM_MODULE_LOCATION)
	string (REGEX REPLACE ".*CPM_([0-9]+\\.[0-9]+\\.[0-9]+)\\.cmake" "\\1" CPM_MODULE_VERSION ${CPM_MODULE_LOCATION})

	if (CPM_MODULE_VERSION VERSION_LESS CPM_DOWNLOAD_VERSION)
		set (CPM_NEEDS_DOWNLOAD TRUE)
	endif ()
endif ()

if (CPM_NEEDS_DOWNLOAD)
	set (CPM_MODULE_LOCATION "${CMAKE_BINARY_DIR}/cmake/CPM_${CPM_DOWNLOAD_VERSION}.cmake")

	message (STATUS "Downloading CPM.cmake")
	file (DOWNLOAD https://github.com/TheLartians/CPM.cmake/releases/download/v${CPM_DOWNLOAD_VERSION}/CPM.cmake
		  ${CPM_MODULE_LOCATION}
	)
endif ()

include (${CPM_MODULE_LOCATION})

include (${CMAKE_CURRENT_LIST_DIR}/GetCommonLibraries.cmake)
include (${CMAKE_CURRENT_LIST_DIR}/BuildDoc.cmake)
include (${CMAKE_CURRENT_LIST_DIR}/IhtaAddTest.cmake)
include (${CMAKE_CURRENT_LIST_DIR}/IhtaAddLibrary.cmake)

macro (find_package)

	if (AVAILABLE_SUB_PROJECTS)
		string (TOLOWER "${ARGV0}" _pkg_low)
		set (_found_in_sub_projects OFF)

		foreach (_sub_project ${AVAILABLE_SUB_PROJECTS})
			string (TOLOWER "${_sub_project}" _sub_low)
			if (${_pkg_low} STREQUAL ${_sub_low})
				set (_found_in_sub_projects ON)
				break ()
			endif ()
		endforeach ()

		if (_found_in_sub_projects)
			message (WARNING "Package ${ARGV0} available as a sub project; skip find_package")
		else ()
			_find_package (${ARGV})
		endif ()
	else ()
		_find_package (${ARGV})
	endif ()

	# if (NOT "${ARGV0}" IN_LIST AVAILABLE_SUB_PROJECTS)
	# 	message (FATAL_ERROR "Package ${ARGV0} not found as a Target; using find_package")
	# 	# _find_package (${ARGV})
	# else ()
	# 	message (FATAL_ERROR "Package ${ARGV0} found as a Target; skip find_package")
	# endif ()
endmacro ()
