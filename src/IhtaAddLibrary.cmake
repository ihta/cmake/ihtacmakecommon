include_guard ()

#[=======================================================================[.rst:
..command:: ihta_add_library

	Macro to create a library including most of the common settings/setups.

	This macro also allows to create a unit test executable with the same command.

	In most cases, after this command only link libraries have to be set and
	optionally install directives.

	Note: when using generated export headers from cmake, these have to be added
	to the lib and test targets manually.

	::

		ihta_add_library(
			NAME <library-name>
			SOURCES <library-source-files...>
			[LIBRARY_TYPE <STATIC|SHARED>]
			[NAMESPACE <target-alias-namespace>]
			[IDE_FOLDER <folder-name-for-IDE>]
			[INCLUDE_DIR <library-include-dir>]
			[INSTALL_INCLUDE_DIR <library-install-include-dir>]
			[OUT_VAR <output-library-target-name-variable>]
			[TEST_SOURCES <test-source-files...>]
			[TEST_INTERNALS]
			[SRC_DIR <library-source-dir>]
			[OBJECT_LIB_POSTFIX <object-lib-postfix>]
		)

	.. variable:: NAME

		Specifies, that the `doxygen-awesome-css` sidebar ony style should be used.

	.. variable:: SOURCES

		Source files for the library.

		If you are using `target_sources` you can use `""` as the sources.
		Todo: test this!

	.. variable:: LIBRARY_TYPE

		Optionally specify the library type. Either `STATIC` or `SHARED`.

		If not specified, this macro behaves similarly to `add_library` in that it
		assumes the type to be `STATIC` unless `BUILD_SHARED_LIBS` is turned on.

	.. variable:: NAMESPACE

		Optional namespace for the alias target of the library.

		At least NAMESPACE or IDE_FOLDER must be given. If only one is given, the other
		one will be set to the value of the given one.

	.. variable:: IDE_FOLDER

		Optional IDE folder for the library target(s).

		At least NAMESPACE or IDE_FOLDER must be given. If only one is given, the other
		one will be set to the value of the given one.

	.. variable:: INCLUDE_DIR

		Optionally specify the libraries include directory.
		Default is `${CMAKE_CURRENT_SOURCE_DIR}/include`.

	.. variable:: INSTALL_INCLUDE_DIR

		Optionally specify the install include directory for the library.
		If not specified, the install expects the same include folder structure as in the
		libraries include folder.

	.. variable:: OUT_LIB

		Variable name into which the library target name will be set.

		This can be useful when testing the internals and an object library is created,
		onto which most further settings must be applied.

	.. variable:: TEST_SOURCES

		Sources files for the optional unit test executable.

	.. variable:: ADD_UNIT_TEST

		If set to true, the unit test will be added.
		Default is `FALSE`.

	.. variable:: TEST_INTERNALS

		Specifies if the unit test should be able to test the internals of the library.

		Under the hood, this then creates an object library for both the library and
		the test to save on compilation time.

		See `cmake_registertest <https://github.com/pthom/cmake_registertest>`_

	.. variable:: SRC_DIR

		Optionally specify the libraries source directory.
		Default is `${CMAKE_CURRENT_SOURCE_DIR}/src`.
		This should be given if TEST_INTERNALS is given and one wants to test the internals.

	.. variable:: OUT_TEST

		Variable name into which the test target name will be set.

		This can be useful when further settings must be applied to the test target.

	.. variable:: OBJECT_LIB_POSTFIX

		Optionally specify a postfix for the object library if TEST_INTERNALS is specified.
		The default is `_object`.

#]=======================================================================]
macro (ihta_add_library)
	set (options TEST_INTERNALS)
	set (
		oneValueArgs
		NAMESPACE
		NAME
		LIBRARY_TYPE
		INCLUDE_DIR
		SRC_DIR
		INSTALL_INCLUDE_DIR
		IDE_FOLDER
		OBJECT_LIB_POSTFIX
		OUT_LIB
		OUT_TEST
		ADD_UNIT_TEST
	)
	set (multiValueArgs SOURCES TEST_SOURCES)
	cmake_parse_arguments (IHTA_ADD_LIB "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

	if (DEFINED IHTA_ADD_LIB_LIBRARY_TYPE)
		set (IHTA_ADD_LIB_TYPES "STATIC" "SHARED")
		if (NOT (${IHTA_ADD_LIB_LIBRARY_TYPE} IN_LIST IHTA_ADD_LIB_TYPES))
			message (FATAL_ERROR "ihta_add_library only supports the following LIBRARY_TYPE's: STATIC, SHARED")
		endif ()
	else ()
		set (IHTA_ADD_LIB_TYPES STATIC)
		if (BUILD_SHARED_LIBS)
			set (IHTA_ADD_LIB_TYPES SHARED)
		endif ()
	endif ()

	if (NOT DEFINED IHTA_ADD_LIB_INCLUDE_DIR)
		set (IHTA_ADD_LIB_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
	endif ()

	if (NOT DEFINED IHTA_ADD_LIB_SRC_DIR)
		set (IHTA_ADD_LIB_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
	endif ()

	if (NOT DEFINED IHTA_ADD_LIB_OBJECT_LIB_POSTFIX)
		set (IHTA_ADD_LIB_OBJECT_LIB_POSTFIX _object)
	endif ()

	if (DEFINED IHTA_ADD_LIB_NAMESPACE AND NOT DEFINED IHTA_ADD_LIB_IDE_FOLDER)
		set (IHTA_ADD_LIB_IDE_FOLDER ${IHTA_ADD_LIB_NAMESPACE})
	elseif (NOT DEFINED IHTA_ADD_LIB_NAMESPACE AND DEFINED IHTA_ADD_LIB_IDE_FOLDER)
		set (IHTA_ADD_LIB_NAMESPACE ${IHTA_ADD_LIB_IDE_FOLDER})
	elseif (NOT DEFINED IHTA_ADD_LIB_NAMESPACE AND NOT DEFINED IHTA_ADD_LIB_IDE_FOLDER)
		message (FATAL_ERROR "ihta_add_library requires to set either a NAMESPACE or an IDE_FOLDER")
	endif ()

	if (IHTA_ADD_LIB_TEST_INTERNALS)
		set (IHTA_ADD_LIB_TARGET ${IHTA_ADD_LIB_NAME}${IHTA_ADD_LIB_OBJECT_LIB_POSTFIX})

		# Create an object library
		add_library (${IHTA_ADD_LIB_TARGET} OBJECT "")

		target_sources (${IHTA_ADD_LIB_TARGET} PRIVATE ${IHTA_ADD_LIB_SOURCES})

		# Add compile definition for the export header generated by cmake's generate_export_header. For example used in
		# packageProject.
		if (${IHTA_ADD_LIB_LIBRARY_TYPE} MATCHES "STATIC")
			target_compile_definitions (${IHTA_ADD_LIB_TARGET} PUBLIC ${IHTA_ADD_LIB_NAME}_STATIC_DEFINE)
		else ()
			target_compile_definitions (${IHTA_ADD_LIB_TARGET} PUBLIC ${IHTA_ADD_LIB_NAME}_EXPORTS)
		endif ()

		# Add the actual library target
		add_library (${IHTA_ADD_LIB_NAME} ${IHTA_ADD_LIB_LIBRARY_TYPE} "")
		target_link_libraries (${IHTA_ADD_LIB_NAME} PUBLIC ${IHTA_ADD_LIB_TARGET})

		add_library (${IHTA_ADD_LIB_NAMESPACE}::${IHTA_ADD_LIB_NAME} ALIAS ${IHTA_ADD_LIB_NAME})

		# Include Directory
		target_include_directories (
			${IHTA_ADD_LIB_NAME}
			PUBLIC $<BUILD_INTERFACE:${IHTA_ADD_LIB_INCLUDE_DIR}>
				   $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${IHTA_ADD_LIB_INSTALL_INCLUDE_DIR}>
		)

		# Required compiler features
		target_compile_features (${IHTA_ADD_LIB_NAME} PUBLIC cxx_std_17)

		# If build out of parent project, add parent folder library to parent project folder
		if (NOT CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
			set_property (TARGET ${IHTA_ADD_LIB_NAME} PROPERTY FOLDER ${IHTA_ADD_LIB_IDE_FOLDER})
		endif ()

		# install the object library so it's in the export set, see:
		# https://gitlab.kitware.com/cmake/cmake/-/issues/18935
		install (TARGETS ${IHTA_ADD_LIB_TARGET} EXPORT ${IHTA_ADD_LIB_NAME}Targets # Adapt this in case package project
				 # is *not* used!
		)

	else ()
		set (IHTA_ADD_LIB_TARGET ${IHTA_ADD_LIB_NAME})

		add_library (${IHTA_ADD_LIB_NAME} ${IHTA_ADD_LIB_LIBRARY_TYPE} "")
		add_library (${IHTA_ADD_LIB_NAMESPACE}::${IHTA_ADD_LIB_NAME} ALIAS ${IHTA_ADD_LIB_NAME})

		target_sources (${IHTA_ADD_LIB_NAME} PRIVATE ${IHTA_ADD_LIB_SOURCES})
	endif ()

	# Include Directory
	target_include_directories (
		${IHTA_ADD_LIB_TARGET}
		PUBLIC $<BUILD_INTERFACE:${IHTA_ADD_LIB_INCLUDE_DIR}>
			   $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${IHTA_ADD_LIB_INSTALL_INCLUDE_DIR}>
	)

	# Required compiler features
	target_compile_features (${IHTA_ADD_LIB_TARGET} PUBLIC cxx_std_17)

	# If build out of parent project, add parent folder library to parent project folder
	if (NOT CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
		set_property (TARGET ${IHTA_ADD_LIB_TARGET} PROPERTY FOLDER ${IHTA_ADD_LIB_IDE_FOLDER})
	endif ()

	# Organize sources in folders
	GroupSourcesByFolder (${IHTA_ADD_LIB_TARGET})

	if (IHTA_ADD_LIB_TEST_SOURCES AND IHTA_ADD_LIB_ADD_UNIT_TEST)
		set (IHTA_ADD_LIB_TEST_TARGET ${IHTA_ADD_LIB_NAME}UnitTest)
		if (IHTA_ADD_LIB_TEST_INTERNALS)
			ihta_add_test (
				NAME ${IHTA_ADD_LIB_TEST_TARGET}
				IDE_FOLDER ${IHTA_ADD_LIB_IDE_FOLDER}
				SOURCES ${IHTA_ADD_LIB_TEST_SOURCES}
				COV_SOURCES ${IHTA_ADD_LIB_INCLUDE_DIR} ${IHTA_ADD_LIB_SRC_DIR}
			)

			target_link_libraries (${IHTA_ADD_LIB_TEST_TARGET} PUBLIC ${IHTA_ADD_LIB_TARGET})

			target_include_directories (
				${IHTA_ADD_LIB_TEST_TARGET}
				PUBLIC $<BUILD_INTERFACE:${IHTA_ADD_LIB_INCLUDE_DIR}>
				PUBLIC $<BUILD_INTERFACE:${IHTA_ADD_LIB_SRC_DIR}>
			)
		else ()
			ihta_add_test (
				NAME ${IHTA_ADD_LIB_TEST_TARGET}
				TEST_TARGET ${IHTA_ADD_LIB_TARGET}
				IDE_FOLDER ${IHTA_ADD_LIB_IDE_FOLDER}
				SOURCES ${IHTA_ADD_LIB_TEST_SOURCES}
			)
		endif ()

		if (IHTA_ADD_LIB_OUT_TEST)
			set (${IHTA_ADD_LIB_OUT_TEST} ${IHTA_ADD_LIB_TEST_TARGET})
		endif ()
	endif ()

	if (IHTA_ADD_LIB_OUT_LIB)
		set (${IHTA_ADD_LIB_OUT_LIB} ${IHTA_ADD_LIB_TARGET})
	endif ()
endmacro ()
