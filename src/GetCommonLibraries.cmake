include_guard ()

CPMAddPackage (
	NAME GrpSrcByDir
	GITHUB_REPOSITORY TheLartians/GroupSourcesByFolder.cmake
	VERSION 1.0
	DOWNLOAD_ONLY YES # Delay the inclusion of this to after the patch
)

if (GrpSrcByDir_ADDED)
	find_package (Git 2.29 REQUIRED)
	execute_process (
		COMMAND ${GIT_EXECUTABLE} apply --check
				"${CMAKE_CURRENT_LIST_DIR}/resources/patches/source_group_rel_path.patch"
		RESULT_VARIABLE patch_ok
		WORKING_DIRECTORY ${GrpSrcByDir_SOURCE_DIR}
		ERROR_QUIET
	)

	if (${patch_ok} STREQUAL "0")
		execute_process (
			COMMAND ${GIT_EXECUTABLE} apply --whitespace=fix
					"${CMAKE_CURRENT_LIST_DIR}/resources/patches/source_group_rel_path.patch"
			RESULT_VARIABLE patch_ok
			WORKING_DIRECTORY ${GrpSrcByDir_SOURCE_DIR}
		)
	endif ()

	# The file is now pathced, add it
	add_subdirectory (${GrpSrcByDir_SOURCE_DIR} ${GrpSrcByDir_BINARY_DIR})
endif ()

CPMAddPackage (
	NAME PkgProj
	VERSION 1.10.0
	GITHUB_REPOSITORY TheLartians/PackageProject.cmake
	DOWNLOAD_ONLY YES # Delay the inclusion of this to after the patch
)

if (PkgProj_ADDED)
	find_package (Git 2.29 REQUIRED)
	execute_process (
		COMMAND ${GIT_EXECUTABLE} apply --check "${CMAKE_CURRENT_LIST_DIR}/resources/patches/change_install_dir.patch"
		RESULT_VARIABLE patch_ok
		WORKING_DIRECTORY ${PkgProj_SOURCE_DIR}
		ERROR_QUIET
	)

	if (${patch_ok} STREQUAL "0")
		execute_process (
			COMMAND ${GIT_EXECUTABLE} apply --whitespace=fix
					"${CMAKE_CURRENT_LIST_DIR}/resources/patches/change_install_dir.patch"
			RESULT_VARIABLE patch_ok
			WORKING_DIRECTORY ${PkgProj_SOURCE_DIR}
		)
	endif ()

	# The file is now pathced, add it
	add_subdirectory (${PkgProj_SOURCE_DIR} ${PkgProj_BINARY_DIR})
endif ()
