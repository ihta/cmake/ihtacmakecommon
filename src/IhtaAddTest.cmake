include_guard ()

#[=======================================================================[.rst:
..command:: ihta_add_test

	Macro to create a unit test executable target including the option to add
	code coverage using OpenCppCoverage.

	This uses catch2 and FakeIt for the unit testing and mocking respectively.

	The macro exposes the option `IHTA_COMMON_ENABLE_CODE_COVERAGE` which can
	disable the code coverage by setting it to `OFF`. This can also be done by
	adding `set(IHTA_COMMON_ENABLE_CODE_COVERAGE OFF)` before including the
	IHTACMakeCommon library.

	::

		ihta_add_test(
			SOURCES <test-source-files...>
			[NAME <readme-file-for-main-page>]
			[TEST_TARGET <target-to-test>]
			[IDE_FOLDER <folder-name-for-IDE>]
			[COV_SOURCES <code-cov-source-dir...>]
		)

	.. variable:: SOURCES

		Source files for the unit test application.

	.. variable:: NAME

		Name of the resulting unit test target. At least NAME or TEST_TARGET
		must be given. When NAME is not given, the unit test will be named:
		`${TEST_TARGET}UnitTest`

	.. variable:: TEST_TARGET

		CMake target of the library under test. At least NAME or TEST_TARGET
		must be given.

	.. variable:: IDE_FOLDER

		Folder under which the unit test target will be listed in IDE's.
		The full folder structure will be: `UnitTest/${IDE_FOLDER}`

	.. variable:: COV_SOURCES

		Paths to the sources of the target under test.
		Used to filter the OpenCppCoverage output.

#]=======================================================================]
macro (ihta_add_test)
	option (IHTA_COMMON_ENABLE_CODE_COVERAGE "Set to false to disable code coverage reporting" ON)

	set (options)
	set (oneValueArgs NAME TEST_TARGET IDE_FOLDER)
	set (multiValueArgs SOURCES COV_SOURCES)
	cmake_parse_arguments (IHTA_ADD_TEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

	if (NOT DEFINED IHTA_ADD_TEST_NAME AND DEFINED IHTA_ADD_TEST_TEST_TARGET)
		set (IHTA_ADD_TEST_NAME ${IHTA_ADD_TEST_TEST_TARGET}UnitTest)
	elseif (NOT DEFINED IHTA_ADD_TEST_NAME AND NOT DEFINED IHTA_ADD_TEST_TEST_TARGET)
		message (FATAL_ERROR "ihta_add_test requires the definition of either the NAME or the TEST_TARGET.")
	endif ()

	# Add required packages
	CPMAddPackage (
		NAME catch2
		GITHUB_REPOSITORY catchorg/Catch2
		VERSION 3.1.0
		OPTIONS "CATCH_INSTALL_DOCS OFF" "BUILD_SHARED_LIBS OFF"
	)

	set_property (TARGET Catch2 Catch2WithMain PROPERTY FOLDER "external_libs")

	CPMAddPackage (
		NAME FakeIt
		GITHUB_REPOSITORY eranpeer/FakeIt
		GIT_TAG 2.3.0
		DOWNLOAD_ONLY ON
	)

	if (IHTA_COMMON_ENABLE_CODE_COVERAGE)
		find_program (OPENCPPCOVERAGE OpenCppCoverage)

		if (OPENCPPCOVERAGE)
			CPMAddPackage (
				NAME cpp_coverage
				GITHUB_REPOSITORY ekcoh/cpp-coverage
				GIT_TAG master
				OPTIONS "CPP_COVERAGE_ENABLE_COV_COMMANDS ON" "CPP_COVERAGE_ENABLE_PER_TARGET_COVERAGE_REPORTS ON"
			)
			# TODO set output types with quotes -> does not work :(

			# unset(CPP_COVERAGE_REPORT_TYPE CACHE)

			# set ( CPP_COVERAGE_REPORT_TYPE "\"Html;Badges\"" CACHE STRING "Report type passed to report tool via --reporttype argument" )
		endif ()
	endif ()

	# Add the test executable
	add_executable (${IHTA_ADD_TEST_NAME} ${IHTA_ADD_TEST_SOURCES})

	target_include_directories (${IHTA_ADD_TEST_NAME} PRIVATE ${FakeIt_SOURCE_DIR}/single_header/catch)

	target_link_libraries (${IHTA_ADD_TEST_NAME} PRIVATE Catch2::Catch2WithMain ${IHTA_ADD_TEST_TEST_TARGET})

	target_compile_features (${IHTA_ADD_TEST_NAME} PUBLIC cxx_std_17)

	set_property (TARGET ${IHTA_ADD_TEST_NAME} PROPERTY FOLDER "UnitTest/${IHTA_ADD_TEST_IDE_FOLDER}")

	if (OPENCPPCOVERAGE AND IHTA_COMMON_ENABLE_CODE_COVERAGE)
		if (DEFINED IHTA_ADD_TEST_TEST_TARGET)
			get_target_property (IHTA_ADD_TEST_TEST_TARGET_SOURCE_DIR ${IHTA_ADD_TEST_TEST_TARGET} SOURCE_DIR)
			list (APPEND IHTA_ADD_TEST_COV_SOURCES "${IHTA_ADD_TEST_TEST_TARGET_SOURCE_DIR}/include"
				  "${IHTA_ADD_TEST_TEST_TARGET_SOURCE_DIR}/src"
			)
		endif ()

		# Set these variables here as well since, it doesn't seems to propagate correctly.
		set (CPP_COVERAGE_ENABLE_COV_COMMANDS ON)
		set (CPP_COVERAGE_ENABLE_PER_TARGET_COVERAGE_REPORTS ON)

		cpp_coverage_add_test (
			TARGET
			${IHTA_ADD_TEST_NAME}
			SOURCES
			${IHTA_ADD_TEST_COV_SOURCES}
			EXCLUDED_SOURCES
			"*out*"
			"*build*"
			REPORT_FOR_GLOBAL
			REPORT_FOR_TARGET
		)

		set_property (TARGET coverage_report PROPERTY FOLDER "coverage")
		set_property (TARGET ${IHTA_ADD_TEST_NAME}_coverage PROPERTY FOLDER "coverage")
		set_property (TARGET ${IHTA_ADD_TEST_NAME}_coverage_report PROPERTY FOLDER "coverage")

		file (TO_NATIVE_PATH ${CMAKE_CURRENT_SOURCE_DIR} IHTA_ADD_TEST_NATIVE_SOURCES)

		add_test (
			NAME "${IHTA_ADD_TEST_NAME}.Global.Global"
			COMMAND
				OpenCppCoverage --export_type=cobertura:${CMAKE_CURRENT_BINARY_DIR}/coverage.xml
				--sources=${IHTA_ADD_TEST_NATIVE_SOURCES} --excluded_sources=*out* --
				$<TARGET_FILE:${IHTA_ADD_TEST_NAME}> -r junit::out=${CMAKE_CURRENT_BINARY_DIR}/result-junit.xml
		)
	else ()
		# use [namespace].[class].[testname] naming
		add_test (NAME "${IHTA_ADD_TEST_NAME}.Global.Global" COMMAND ${IHTA_ADD_TEST_NAME} -r junit::out=${CMAKE_CURRENT_BINARY_DIR}/result-junit.xml) # todo fix namespace name
	endif ()
endmacro ()
