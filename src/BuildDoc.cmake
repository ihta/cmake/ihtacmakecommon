include_guard ()

find_package (
	Doxygen
	COMPONENTS dot
	OPTIONAL_COMPONENTS mscgen dia
)

set (
	DOXYGEN_FOUND
	${DOXYGEN_FOUND}
	PARENT_SCOPE
)

set (IHTA_CMAKE_COMMON_DOCSTYLE_LANDED ${CMAKE_CURRENT_LIST_DIR}/resources/landed.css)
set (IHTA_CMAKE_COMMON_DOCSTYLE_RWTH ${CMAKE_CURRENT_LIST_DIR}/resources/rwth.css)
set (IHTA_CMAKE_COMMON_DOC_LOGO ${CMAKE_CURRENT_LIST_DIR}/resources/ihta.svg)

set (
	IHTA_CMAKE_COMMON_DOCSTYLE_LANDED
	${IHTA_CMAKE_COMMON_DOCSTYLE_LANDED}
	PARENT_SCOPE
)
set (
	IHTA_CMAKE_COMMON_DOCSTYLE_RWTH
	${IHTA_CMAKE_COMMON_DOCSTYLE_RWTH}
	PARENT_SCOPE
)
set (
	IHTA_CMAKE_COMMON_DOC_LOGO
	${IHTA_CMAKE_COMMON_DOC_LOGO}
	PARENT_SCOPE
)

#[=======================================================================[.rst:
..command:: build_doc

	Macro to generate doxygen documentation with custom logo and style sheet.

	::

		build_doc(
			NAME genDocTarget
			SOURCES <documentation-folder...>
			[README_MAINPAGE <readme-file-for-main-page>]
			[COLOR_STYLE <css-style-sheet>]
			[LOGO <logo-file>]
			[ADD_DEV_SECTION]
			[SIDEBAR_ONLY]
		)

	.. variables:: NAME

		Target name for building the documentation.
		Default is `build_doc`

	.. variable:: SOURCES

		List of folders or files to be included in the doxygen documentation

	.. variable:: README_MAINPAGE

		Specify a specific README markdown file as the mainpage for the documentation.

	.. variable:: COLOR_STYLE

		``css`` style sheet with a custom color style.
		The definition must follow the `doxygen-awesome-css <https://github.com/jothepro/doxygen-awesome-css>`_ definition.
		Default is the included `landed` style.

	.. variable:: LOGO

		Logo file for the documentation.
		Used in the header as well as the icon in the tab.
		Default is the included IHTA logo.

	.. variable:: ADD_DEV_SECTION

		If true, the `DEV` section will be enabled in doxygen.
		If the documentation is set up correctly, parts of the documentation can be hidden with this.
		This means, adding a `\cond DEV` in the file scope documentation comment.

	.. variable:: SIDEBAR_ONLY

		Specifies, that the `doxygen-awesome-css` sidebar ony style should be used.


#]=======================================================================]
macro (build_doc)
	set (options SIDEBAR_ONLY)
	set (oneValueArgs README_MAINPAGE COLOR_STYLE LOGO ADD_DEV_SECTION NAME)
	set (multiValueArgs SOURCES)
	cmake_parse_arguments (DOXY "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

	if (NOT DOXY_NAME)
		set (DOXY_NAME build_doc)
	endif ()

	if (DOXYGEN_FOUND)
		CPMAddPackage (
			NAME doxygen-awesome
			GITHUB_REPOSITORY jothepro/doxygen-awesome-css
			VERSION 2.1.0
			DOWNLOAD_ONLY YES
		)

		execute_process (
			COMMAND ${DOXYGEN_EXECUTABLE} -w html ${CMAKE_BINARY_DIR}/header ${CMAKE_BINARY_DIR}/footer
					${CMAKE_BINARY_DIR}/style
		)
		file (READ ${CMAKE_BINARY_DIR}/header doc_header_tmp)
		string (
			REPLACE
				"</head>"
				[[<script type="text/javascript" src="$relpath^doxygen-awesome-darkmode-toggle.js"></script>
<script type="text/javascript" src="$relpath^doxygen-awesome-paragraph-link.js"></script>
<script type="text/javascript">
	DoxygenAwesomeDarkModeToggle.init()
	DoxygenAwesomeParagraphLink.init()
</script>
<!--BEGIN PROJECT_LOGO-->
<link rel="shortcut icon" href="$relpath^$projectlogo" type="image/x-icon" />
<!--END PROJECT_LOGO-->
</head>]]
				doc_header_tmp
				${doc_header_tmp}
		)

		file (WRITE ${CMAKE_BINARY_DIR}/header ${doc_header_tmp})
		unset (doc_header_tmp)

		set (DOXYGEN_GENERATE_TREEVIEW YES)
		set (DOXYGEN_HTML_EXTRA_STYLESHEET ${doxygen-awesome_SOURCE_DIR}/doxygen-awesome.css)

		if (DOXY_SIDEBAR_ONLY)
			list (APPEND DOXYGEN_HTML_EXTRA_STYLESHEET ${doxygen-awesome_SOURCE_DIR}/doxygen-awesome-sidebar-only.css
				  ${doxygen-awesome_SOURCE_DIR}/doxygen-awesome-sidebar-only-darkmode-toggle.css
			)
		endif ()

		if (DEFINED DOXY_COLOR_STYLE)
			list (APPEND DOXYGEN_HTML_EXTRA_STYLESHEET ${DOXY_COLOR_STYLE})
		else ()
			list (APPEND DOXYGEN_HTML_EXTRA_STYLESHEET ${IHTA_CMAKE_COMMON_DOCSTYLE_LANDED})
		endif ()

		set (DOXYGEN_HTML_COLORSTYLE_HUE 209)
		set (DOXYGEN_HTML_COLORSTYLE_SAT 255)
		set (DOXYGEN_HTML_COLORSTYLE_GAMMA 113)
		set (DOXYGEN_DOT_IMAGE_FORMAT svg)
		set (DOXYGEN_DOT_TRANSPARENT YES)
		set (DOXYGEN_INTERACTIVE_SVG YES)

		if (DEFINED DOXY_README_MAINPAGE)
			set (DOXYGEN_USE_MDFILE_AS_MAINPAGE ${DOXY_README_MAINPAGE})
			list (APPEND DOXY_SOURCES ${DOXY_README_MAINPAGE})
		endif ()

		set (DOXYGEN_HTML_HEADER ${CMAKE_BINARY_DIR}/header)
		set (DOXYGEN_HTML_EXTRA_FILES ${doxygen-awesome_SOURCE_DIR}/doxygen-awesome-darkmode-toggle.js
									  ${doxygen-awesome_SOURCE_DIR}/doxygen-awesome-paragraph-link.js
		)

		if (DEFINED DOXY_LOGO)
			set (DOXYGEN_PROJECT_LOGO ${DOXY_LOGO})
			list (APPEND DOXYGEN_HTML_EXTRA_FILES ${DOXY_LOGO})
		else ()
			set (DOXY_LOGO ${IHTA_CMAKE_COMMON_DOC_LOGO})
			set (DOXYGEN_PROJECT_LOGO ${DOXY_LOGO})
			list (APPEND DOXYGEN_HTML_EXTRA_FILES ${DOXY_LOGO})
		endif ()

		if (DOXY_ADD_DEV_SECTION)
			set (DOXYGEN_ENABLED_SECTIONS DEV)
			set (DOXYGEN_EXTRACT_PRIVATE YES)
		endif ()

		doxygen_add_docs (${DOXY_NAME} ${DOXY_SOURCES} ALL)

		set_property (TARGET ${DOXY_NAME} PROPERTY FOLDER "utils")

	endif ()

endmacro ()
