# IHTA CMake Common

A collection of CMake helper functions and tools that are common to all IHTA C++ Projects.

## Features

- `ihta_add_library`: Adds a library target with all common settings.
  This also allows to add a unit test executable with the same command.
  Furthermore, if unit tests are desired, one can also use the option `TEST_INTERNALS` to be able to test the internal parts of the library.
- Checks if [`CPM.cmake`](https://github.com/cpm-cmake/CPM.cmake) is available and downloads it if not.
  For this, it expects the `CPM.cmake` file to be in `${CMAKE_BINARY_DIR}/cmake`.
- Download some common CMake libraries in [`GroupSourcesByFolder.cmake`](https://github.com/TheLartians/GroupSourcesByFolder.cmake) and [`PackageProject.cmake`](https://github.com/TheLartians/PackageProject.cmake) and patches them for our build system.
- `init_project`: Initializes some common settings for a project.
  Primarily sets the output `bin` and `lib` directories so debugging works on windows builds.
- `ihta_add_test`: Adds a unit test executable target including downloading the dependencies, CTest registration and optional code coverage.
- `build_doc`: Builds doxygen documentation with nice styling.

## Usage

Here is an example of the usage:

```cmake
cmake_minimum_required(VERSION 3.20)

# optionally disable code coverage
set(IHTA_COMMON_ENABLE_CODE_COVERAGE ON)

include (FetchContent)
FetchContent_Declare (
    IHTACMakeCommon
    GIT_REPOSITORY https://git.rwth-aachen.de/ihta/cmake/ihtacmakecommon.git
    GIT_TAG main
)
FetchContent_MakeAvailable (IHTACMakeCommon)

init_project ()

option (PROJECT_WITH_UNIT_TESTS "Add unit tests for project" OFF)

project (AwesomeProjectName
    LANGUAGES CXX C
    VERSION 1.0.0
)

CPMAddPackage("gh:gabime/spdlog@1.8.2")

# Add the library target with unit test
ihta_add_library (
    NAME ${PROJECT_NAME}
    SOURCES include/project.h src/project.cpp
    NAMESPACE IHTA
    IDE_FOLDER ${PROJECT_NAME}
    LIBRARY_TYPE STATIC
    OUT_LIB LIB_TARGET          # The name of the library target will be stored in `LIB_TARGET`. Useful for adding dependencies to the target. See below.
    OUT_TEST TEST_TARGET        # The name of the unit test target will be stored in `TEST_TARGET`. Useful for adding dependencies to the target. See below.
    TEST_SOURCES test/test.cpp  # This adds a unit test with the given source files.
    TEST_INTERNALS              # With this option, the internals of the library can be unit tested as well.
    ADD_UNIT_TEST ${PROJECT_WITH_UNIT_TESTS} # This option can enable or disable the unit tests.
)

# Add external library link to library
target_link_libraries(${LIB_TARGET} PRIVATE spdlog)

# Add external library link to unit test
target_link_libraries(${TEST_TARGET} PRIVATE OtherExternalLibrary)

# Install & export
packageProject (
    NAME ${PROJECT_NAME}
    VERSION ${PROJECT_VERSION}
    BINARY_DIR ${PROJECT_BINARY_DIR}
    INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include
    INCLUDE_DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    DEPENDENCIES "ExternalLibrary"
    COMPATIBILITY ExactVersion
    NAMESPACE IHTA
    DISABLE_VERSION_SUFFIX YES
    VERSION_HEADER "AwesomeProjectName_version.h"
    EXPORT_HEADER "AwesomeProjectName_export.h"
)

# Needed so the object library knows the location of the generated headers from packageProject.
target_include_directories (${LIB_TARGET} PUBLIC "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/PackageProjectInclude>")

if (PROJECT_WITH_UNIT_TESTS)
    target_include_directories (${TEST_TARGET} PUBLIC "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/PackageProjectInclude>")
endif ()

build_doc (
    ADD_DEV_SECTION OFF
    README_MAINPAGE ${CMAKE_CURRENT_SOURCE_DIR}/README.md
    SOURCES "include" "src" "docs"
)
```

## References

- [cpp_coverage](https://github.com/ekcoh/cpp-coverage/blob/master/cmake/cpp_coverage.cmake)
- [crt_registertest](https://github.com/pthom/cmake_registertest/blob/master/src/crt_registertest.cmake)
